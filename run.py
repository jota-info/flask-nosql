#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""Todo list baseada em Python3.4, Flask, Flask Restful, e MongoDB"""
from lista_tarefas import app

if __name__ == '__main__':
    app.run(debug=True)
