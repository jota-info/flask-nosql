#!/usr/bin/env python
# -*- coding: utf-8 -*-

from flask_restful import Resource
from lista_tarefas.configuracoes.settings import api
from lista_tarefas.controller.categorias import Categorias, Categoria
from lista_tarefas.controller.tarefas import Tarefa, Tarefas
from lista_tarefas.controller.estatisticas import Estatisticas, Estatisticas_tarefas, Estatisticas_categorias

#Urls
# Categorias
api.add_resource(Categorias, '/categorias')
api.add_resource(Categoria, '/categorias/<id_categoria>')
# Tarefas
api.add_resource(Tarefa, '/categorias/tarefas/<id_tarefa>')
api.add_resource(Tarefas, '/categorias/tarefas')
# Estatísticas
api.add_resource(Estatisticas, '/estatisticas')
api.add_resource(Estatisticas_categorias, '/estatisticas/categorias')
api.add_resource(Estatisticas_tarefas, '/estatisticas/tarefas')