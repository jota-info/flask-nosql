#!/usr/bin/env python
# -*- coding: utf-8 -*-

from flask import Flask
from pymongo import MongoClient
from flask_restful import Api

#Declarações fundamentais
app = Flask(__name__)
api = Api(app)

#Configurações básicas da base de dados
cliente = MongoClient()
banco = cliente.lista_tarefas

# Define o Access-Control-Allow para Clientes terem acesso a API
@app.after_request
def after_request(response):
    """Ativação de permissões de acesso aos métodos HTTP"""
    response.headers.add('Access-Control-Allow-Origin', '*')
    response.headers.add(
        'Access-Control-Allow-Headers', 'Content-Type, Authorization'
    )
    response.headers.add('Access-Control-Allow-Methods', 'GET, PUT, POST, DELETE')
    return response

import lista_tarefas.controller.categorias
import lista_tarefas.controller.tarefas
import lista_tarefas.controller.estatisticas
import lista_tarefas.configuracoes.urls.urls