#!/usr/bin/env python
# -*- coding: utf-8 -*-

from flask import jsonify, request
from flask.views import MethodView
from bson.objectid import ObjectId
from lista_tarefas.configuracoes.settings import banco 
from lista_tarefas.controller.serializer import serializer_categorias

class Categorias(MethodView):
    """Classe destinada a ações para todas as categorias"""
    def get(self):
        """Retorna todas as categorias"""
        #Inclusão de limite opcional
        if request.args.get('limite'):
            try:
                cursor = banco.categorias.find({}).limit(int(request.args.get('limite')))
            except:
                return jsonify({'Erro':'Parâmetro inválido'})
        else:
            cursor = banco.categorias.find({})
        resultados = []
        for documento in cursor:
            documento['_id'] = str(documento['_id'])
            resultados.append(documento)
        return resultados

    def post(self):
        """Adiciona nova categoria"""
        args = serializer_categorias()
        banco.categorias.insert_one({'nome': args['nome'], 'descricao': args['descricao']})
        return jsonify({'resultado': 'Ok!'})

    def delete(self):
        """Deleta TODAS as categorias"""
        banco.categorias.delete_many({})
        return jsonify({'resultado': 'Ok!'})

class Categoria(MethodView):
    """Classe destinada a ações específicas de determinada categoria"""
    def get(self, id_categoria):
        """Retorna apenas informações a respeito de uma categoria"""
        cursor = banco.categorias.find_one({'_id': ObjectId(id_categoria)})
        cursor['_id'] = str(cursor['_id'])
        return cursor

    def put(self, id_categoria):
        """Altera uma categoria específica"""
        args = serializer_categorias()
        cursor = banco.categorias.update_one(
            {'_id': ObjectId(id_categoria)},
            {'$set': {'nome': args['nome'], 'descricao': args['descricao']}},
            upsert=True
        )
        return jsonify({'resultado': 'Ok!'})

    def delete(self, id_categoria):
        """Deleta uma categoria específica"""
        banco.categorias.delete_one({'_id': ObjectId(id_categoria)})
        return jsonify({'resultado': 'Ok!'})
