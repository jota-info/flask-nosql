#!/usr/bin/env python
# -*- coding: utf-8 -*-

from flask import jsonify, request
from flask.views import MethodView
from bson.objectid import ObjectId
from lista_tarefas.configuracoes.settings import banco 
from lista_tarefas.controller.serializer import serializer_categorias

class Estatisticas(MethodView):
    """Classe destinada a estatísticas gerais"""
    def get(self):
        return banco.command('dbstats')

class Estatisticas_categorias(MethodView):
    """Classe destinada a estatísitcas da coleção categorias"""
    def get(self):
        return banco.command('collstats', 'categorias')

class Estatisticas_tarefas(MethodView):
    """Classe destinada a estatísticas da coleção tarefas"""
    def get(self):
        return banco.command('collstats', 'categorias')
