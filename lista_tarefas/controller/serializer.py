#!/usr/bin/env python
# -*- coding: utf-8 -*-

from pymongo import MongoClient
from flask import Flask
from flask_restful import Api, reqparse

def serializer_categorias():
    """Serializa os dados que são recebidos como parâmetros

    utilizando o formado Json para a coleção categorias
    """
    parser = reqparse.RequestParser()
    parser.add_argument('nome', type=str)
    parser.add_argument('descricao', type=str)
    return parser.parse_args()

def serializer_tarefas():
    """Serializa os dados que são recebidos como parâmetros

    utilizando o formado Json para a coleção tarefas
    """
    parser = reqparse.RequestParser()
    parser.add_argument('titulo', type=str)
    parser.add_argument('descricao', type=str)
    parser.add_argument('prioridade', type=str)
    parser.add_argument('categoria', type=str)
    return parser.parse_args()
