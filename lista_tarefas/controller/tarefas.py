#!/usr/bin/env python
# -*- coding: utf-8 -*-

from flask import jsonify
from flask.views import MethodView
from bson.objectid import ObjectId
from lista_tarefas.configuracoes.settings import banco
from lista_tarefas.controller.serializer import serializer_tarefas


class Tarefas(MethodView):
    """Classe destinada a ações para todas as tarefas"""
    def get(self):
        """Retorna todas as tarefas"""
        cursor = banco.tarefas.find({})
        resultados = []
        for documento in cursor:
            documento['_id'] = str(documento['_id'])
            resultados.append(documento)
        return resultados

    def post(self):
        """Adiciona nova tarefa"""
        args = serializer_tarefas()
        banco.tarefas.insert_one({'titulo': args['titulo'], 'descricao': args['descricao'], 'prioridade': args['prioridade'], 'categoria': args['categoria']})
        return jsonify({'resultado': 'Ok!'})

    def delete(self):
        """Elimina TODAS as tarefas"""
        banco.tarefas.delete_many({})
        return jsonify({'resultado': 'Ok!'})

class Tarefa(MethodView):
    """Classe destinada a ações específicas sobre tarefas"""
    def get(self, id_tarefa):
        """Retorna uma tarefa específica"""
        cursor = banco.tarefas.find_one({'_id': ObjectId(id_tarefa)})
        cursor['_id'] = str(cursor['_id'])
        return cursor

    def put(self, id_tarefa):
        """Altera uma tarefa específica"""
        args = serializer_tarefas()
        cursor = banco.tarefas.update_one(
            {'_id': ObjectId(id_tarefa)},
            {'$set': {'titulo': args['titulo'], 'descricao': args['descricao'], 'prioridade': args['prioridade'], 'categoria': ['categoria']}},
            upsert=True
        )
        return jsonify({'resultado': 'Ok!'})

    def delete(self, id_tarefa):
        """Deleta uma tarefa específica"""
        banco.tarefas.delete_one({'_id': ObjectId(id_tarefa)})
        return jsonify({'resultado': 'Ok!'})
