# LEIA ME #

### TODO-List fazendo uso de Python3, MongoDB, Flask e Flask-restful ###

Os objetivos são: 

* Estudar MongoDB
* Verificar como fazer uma integração com Flask
* Estudar Pymongo
* Estudar boas práticas Python

### Como configurar ###

* Crie um ambiente virtual com Python3 e virtualenv e instale as dependências

```
#Debian/Ubuntu/Mint/.../
sudo apt-get install python3-virtualenv
virtualenv3 nome_do_ambiente_virtual
source /caminho/para/nome_do_ambiente_virtual/bin/activate
pip3 install flask-restful
pip3 install pymongo

#RedHat/CentOS/Fedora/.../
sudo yum(dnf no caso do Fedora 21 ou +) install python3-virtualenv
virtualenv3 nome_do_ambiente_virtual
source /caminho/para/nome_do_ambiente_virtual/bin/activate
pip3 install flask-restful
pip3 install pymongo

```
* Com o ambiente ativo, navegue pelo também pelo terminal até run.py dentro do projeto e digite o seguinte comando:
```
python3 app.py
```

### Testes ###

* ####curl####
	* Listar todas as tarefas
		```curl http://localhost:{port}/categorias/tarefas```
	* Listar todas as categorias
		```curl http://localhost:{port}/categorias
			ou pode-se adicionar um limite de respostas à consulta:
		   curl http://localhost:{port}/categorias?limite=10
		```
	* Lista uma tarefa específica
		```curl http://localhost:{port}/categorias/tarefas/{id tarefa}```
	* Lista uma categoria específica
		```curl http://localhost:{port}/categorias/{id categoria}```
	* Adicionar uma nova tarefa
		```curl -X POST -H "Content-Type: application/json" -d '{"titulo": "Exemplo titulo", "descricao": "exemplo_descricao", "categoria":"NomeCategoria"}' http://localhost:{port}/categorias/tarefas```
	* Adicionar uma nova categoria
		```curl -X POST -H "Content-Type: application/json" -d '{"nome": "ExemploNomeCategoria", "descricao": "exemplo_descricao"}' http://localhost:{port}/categorias```
	* Deletar todas as tarefas
		```curl -X DELETE http://localhost:{port}/categorias/tarefas```
	* Deletar uma tarefa
		```curl -X DELETE http://localhost:{port}/categorias/tarefas/{id tarefa}```
	* Deletar todas as Categorias
		```curl -X DELETE http://localhost:{port}/categorias```
	* Deletar uma categoria específica
		```curl -X DELETE http://localhost:{port}/categorias/{id categoria}```
	* Alterar uma tarefa
		```curl -X PUT -H "Content-Type: application/json" -d '{"titulo": "Exemplo titulo", "descricao": "exemplo_descricao", "categoria":"Nome Categoria"}' http://localhost:{port}/categorias/tarefas/{id tarefa}```
	* Alterar uma categoria
		```curl -X PUT -H "Content-Type: application/json" -d '{"nome": "ExemploNome", "descricao": "exemplo_descricao"}' http://localhost:{port}/categorias/{id categoria}```



### E se eu não tiver entendido nada? ###

* Entre em contato comigo via e-mail! j.h.o.junior12@gmail.com
* O objetivo de ser opensource e em português é ajudar outras pessoas que também querem descobrir como trabalhar com essas ferramentas